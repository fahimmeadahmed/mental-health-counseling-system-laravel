<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

{{--    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">--}}
{{--    <script src="{{asset('js/jquery.js')}}" defer type="application/javascript"></script>--}}
{{--    <script src="{{asset('js/popper.js')}}" defer type="application/javascript"></script>--}}
{{--    <script src="{{ asset('js/bootstrap.js') }}" type="application/javascript"></script>--}}
        <script src="{{asset('js/app.js')}}" defer></script>
        <script src="{{asset('js/staffScript.js')}}" defer type="application/javascript"></script>
{{--    <script src="{{asset('js/staffForumScript.js')}}" defer type="application/javascript"></script>--}}


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/all.css')}}" rel="stylesheet">

</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>staff</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        fakeimg {
            height: 200px;
            background: #aaa;
        }
    </style>
</head>
<body>

<div class="jumbotron text-center" style="margin-bottom:0">
    <h1>Mental Health Counseling System</h1>
    <h3 style="color:blue;">Welcome to Staff Portal</h3>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="{{route('staff.index')}}">Staff</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{route('staff.userList.index',Auth::user()->id)}}">User List</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('staff.doctorList.index',Auth::user()->id)}}">Doctor List</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('staff.forum.index',Auth::user()->id)}}">Forum</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('staff.feedback.index',Auth::user()->id)}}">Feedback</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>

</body>
</html>


<main class="py-4">
        @yield('content')
    </main>

</div>
</body>
</html>
