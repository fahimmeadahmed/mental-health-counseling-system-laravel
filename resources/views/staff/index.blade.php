@extends('layouts.staff')
@section('content')
<div class="container" style="margin-top:30px">
    <div class="row">

        <div class="col-sm-4">
            <h2>STAFF: {{$user->s_id}}</h2>
            <img src="{{asset('img/'.$user->photo)}}" class="rounded mx-auto d-block img-thumbnail" style="width: auto;height: 200px">
            <div class="card-body text-center">
                <div class="h5 text-left">
                    <div class="row">
                        <div class="col-6 text-right">Name: </div>
                        <div class="col-6">{{$user->name}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6 text-right">Phone: </div>
                        <div class="col-6">{{$user->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-6 text-right">Gender: </div>
                        <div class="col-6">{{$user->gender}}</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
