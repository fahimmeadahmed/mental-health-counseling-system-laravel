@extends('layouts.staff')
@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>Delete User</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
</head>
<body>
    <form method="post" action="{{route('staff.userList.destroy',[Auth::user()->id,$user->id])}}">
		@csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{$user->id}}">
        <div class="container">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User TYpe</th>
                </tr>
                </thead>
                <tbody>

                <tr class="table-danger">
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['type']}}</td>

		<h3>Do you really want to delete this user?</h3>
		<input type="submit" name="submit" value="Confirm">

                    <a href="{{route('staff.userList.index',Auth::user()->id)}}">

                    <button type="button" class="btn btn-success"> Back </button></a>
	</form>
</body>
</html>
@endsection
