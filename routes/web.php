<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');//->middleware('sess');
Route::resource('staff',\App\Http\Controllers\StaffController::class);
Route::resource('staff.userList',\App\Http\Controllers\StaffUserListController::class);
Route::resource('staff.doctorList',\App\Http\Controllers\StaffDoctorListController::class);
Route::get('userList/{userList}/delete/{id}',[App\Http\Controllers\StaffUserListController::class,'delete'])->name('userList.delete');
Route::resource('staff.forum',\App\Http\Controllers\StaffForumController::class);
Route::resource('staff.forum.comment',\App\Http\Controllers\StaffCommentController::class);
Route::resource('staff.feedback',\App\Http\Controllers\StaffFeedbackController::class);
Route::put('/staff/{staff}/doctorList/{doctorList}/ban/{id}',[App\Http\Controllers\StaffDoctorListController::class,'ban'])->name('doctorList.ban');

